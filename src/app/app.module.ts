import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { RouterModule, Routes } from '@angular/router';
import { TablesComponent } from './tables/tables.component';
import { NavigationComponent } from './navigation/navigation.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ComfirmationComponent } from './comfirmation/comfirmation.component';
import { AvatarComponent } from './avatar/avatar.component';
import { FoodComponent } from './food/food.component';

const appRoutes: Routes = [
  { path: '', component:AuthComponent},
  { path: 'auth/tables', component: TablesComponent},
  { path: 'dashboard/:tableNumber', component: DashboardComponent},
  { path: 'comfirmation', component: ComfirmationComponent}

]

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    TablesComponent,
    NavigationComponent,
    DashboardComponent,
    ComfirmationComponent,
    AvatarComponent,
    FoodComponent,

  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule,
    Ng2SearchPipeModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
