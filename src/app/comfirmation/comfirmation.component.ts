import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-comfirmation',
  templateUrl: './comfirmation.component.html',
  styleUrls: ['./comfirmation.component.css']
})
export class ComfirmationComponent implements OnInit {

  choosenFoods: any=[];
  choosenFoodsPriceTotal

  constructor(private _http: HttpClient, private _route: Router) {

    this.choosenFoods = this._route.getCurrentNavigation().extras.state.data;

  }

  ngOnInit(): void {

    console.log(this.choosenFoods);
    this.choosenFoodsPriceTotal = this.choosenFoods.reduce((a, b) => a + (parseInt(b['foodPrice']) || 0), 0);

  }

  // sum(key) {
  //   return this.choosenFoods.reduce((a, b) => a + (b[key] || 0), 0);
  // }

}
