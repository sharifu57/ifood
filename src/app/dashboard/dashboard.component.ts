import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AppConfig } from '../config';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  foods: any=[];
  food:any={};
  tables: any=[];
  choosenFoods: any=[];
  choosenFoodsPriceTotal;
  searchFoods;
  tableNumber: string;
  totalPrice: string;
  dashbboardResponse;

  constructor(private _http: HttpClient, private _route: Router, private _activatedRoute:ActivatedRoute) { }

  ngOnInit(): void {
    this.getData()

    this._activatedRoute.params.subscribe((params: Params) => {
      this.tableNumber = params.tableNumber
    });
  }

  getData(){
    this._http.get(AppConfig.API_ENDPOINT+'/api/v1/food/getpictures')
    .subscribe(
      data=>{
        this.foods = data;
        console.log(this.foods)
      }
    )
  }

  sum(key) {
    return this.choosenFoods.reduce((a, b) => a + (b[key] || 0), 0);
  }


  foodSelected(selectedFood) {
    this.choosenFoods.push(selectedFood);
    this.choosenFoodsPriceTotal = this.choosenFoods.reduce((a, b) => a + (parseInt(b['foodPrice']) || 0), 0);
    console.log(this.choosenFoods);
  }

  removeChosenFood(indexPosition){
    this.choosenFoods.splice(indexPosition, 1);
    this.choosenFoodsPriceTotal = this.choosenFoods.reduce((a, b) => a + (parseInt(b['foodPrice']) || 0), 0);
    console.log(this.choosenFoods);
  }

  navigateToInput() {
    console.log(this.choosenFoods);

    var dataPost = {
      'tableNumber': this.tableNumber,
      'choosenFoods': this.choosenFoods,
      'totalPrice': this.choosenFoodsPriceTotal,
    }

    console.log(dataPost);

    this._http.post(AppConfig.API_ENDPOINT+'/api/v1/order/create/',dataPost)
    .subscribe(
      data => {
        this.dashbboardResponse = data;
        console.log(this.dashbboardResponse)

        if(this.dashbboardResponse.message == 'success'){
          console.log('success')
          this._route.navigate(['/comfirmation'], {state: {data: this.choosenFoods}});
        }else{
          console.log('failed')
        }
      }
    )





    // this._route.navigate(['/comfirmation'], {state: {data: this.choosenFoods}});
    // console.log('Here');
  }


}
