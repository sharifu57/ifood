import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AppConfig } from '../config';


@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.css']
})
export class TablesComponent implements OnInit {
  tables: any=[];
  table: any={};
  tableResponse;
  searchTables;



  constructor(private _http: HttpClient, private _route: Router) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this._http.get(AppConfig.API_ENDPOINT+'/api/v1/table/get/all/table')
    .subscribe(
      data=>{
        this.tables = data;
        console.log(this.tables)
      }
    )
  }

  viewTable(selectedTable){
    this.table = selectedTable;
    // this._route.navigate(['/dashboard/'], {state: {data: this.tables}});
    console.log('Here');

    // this._http.post(AppConfig.API_ENDPOINT+'/api/v1/table/status/activate/'+ JSON.parse(localStorage.setItem('table')).id)
    // .subscribe(
    //   data=>{
    //     this.tableResponse = data;
    //     console.log(this.tableResponse)

    //     if(this.tableResponse.message == 'success'){
    //       console.log('success')
    //       localStorage.setItem('tableNumber',  JSON.stringify(this.table));
    //       this._route.navigate(['/dashboard']);
    //     }else{
    //       console.log('failed')
    //     }
    //   }
    // )

  }

}
